{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    ...
  }: let
    supportedSystems = ["x86_64-linux"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
  in {
    devShells =
      forAllSystems
      (system: let
        poetry2nix = inputs.poetry2nix.lib.mkPoetry2Nix {pkgs = pkgs.${system};};
      in {
        default =
          pkgs.${system}.mkShellNoCC
          {
            propogatedBuildInputs = with pkgs.${system}; [
              python312Packages.setuptools
            ];

            packages = with pkgs.${system}; [
              (poetry2nix.mkPoetryApplication {
                projectDir = self;
                overrides = poetry2nix.overrides.withDefaults (final: prev: {
                  kdl-py = prev.kdl-py.overridePythonAttrs (old: {
                    buildInputs = (old.buildInputs or []) ++ [prev.setuptools];
                  });
                  cryptography = prev.cryptography.overridePythonAttrs (old: {
                    buildInputs = (old.buildInputs or []) ++ [prev.distutils];
                  });
                });
              })
              poetry
            ];
          };
      });
  };
}
