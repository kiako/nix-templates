{
  description = "Kaylee's Nix templates!";

  outputs = _: {
    templates = {
      bare = {
        path = ./bare;
        description = "Bare development environment.";
      };

      bevy = {
        path = ./bevy;
        description = "Rust with Bevy dependencies";
      };

      dart = {
        path = ./dart;
        description = "Dart";
      };

      dart-flutter = {
        path = ./dart-flutter;
        description = "Flutter app template";
        welcomeText = ''
          Start by running `flutter create .`!
        '';
      };

      deno = {
        path = ./deno;
        description = "Deno";
      };

      go = {
        path = ./go;
        description = "Go";
      };

      kotlin = {
        path = ./kotlin;
        description = "Kotlin";
      };

      kotlin-android = {
        path = ./kotlin-android;
        description = "Android SDK with Kotlin";
      };

      lua = {
        path = ./lua;
        description = "Lua 5.4";
      };

      node = {
        path = ./node;
        description = "NodeJS and Yarn";
      };

      python = {
        path = ./python;
        description = "Python with Poetry";
      };

      rust = {
        path = ./rust;
        description = "Latest stable Rust";
      };

      rust-fullstack = {
        path = ./rust-fullstack;
        description = "Rust fullstack using Axum and Yew";
      };

      typescript = {
        path = ./typescript;
        description = "TypeScript";
      };

      typst = {
        path = ./typst;
        description = "Typst";
      };
    };
  };
}
