use axum::{routing::get, Router};
use clap::Parser;
use std::net::{IpAddr, Ipv6Addr, SocketAddr};
use std::str::FromStr;
use tower::ServiceBuilder;
use tower_http::{
    services::{ServeDir, ServeFile},
    trace::TraceLayer,
};

#[derive(Parser, Debug)]
#[clap(name = "server", about = "A server for our wasm project!")]
struct Opt {
    /// The log level; one of "error", "warn", "info", "debug", or "trace".
    #[clap(long = "log", default_value = "info")]
    log_level: tracing::Level,

    /// The listening address.
    #[clap(long = "addr", default_value = "::1")]
    addr: String,
    /// The listening port.
    #[clap(long = "port", default_value = "8080")]
    port: u16,

    /// The directory to serve static files from.
    #[clap(long = "static-dir", default_value = "./dist")]
    static_dir: String,
}

#[tokio::main]
async fn main() {
    let opt = Opt::parse();

    tracing_subscriber::fmt()
        .with_max_level(opt.log_level)
        .init();

    let serve_dir = ServeDir::new("dist").not_found_service(ServeFile::new("dist/index.html"));

    let app = Router::new()
        .route("/api/hello", get(hello))
        .fallback_service(serve_dir)
        .layer(ServiceBuilder::new().layer(TraceLayer::new_for_http()));

    let sock_addr = SocketAddr::from((
        IpAddr::from_str(opt.addr.as_str()).unwrap_or(IpAddr::V6(Ipv6Addr::LOCALHOST)),
        opt.port,
    ));

    tracing::info!("Listening at: http://{}", sock_addr);

    let listener = tokio::net::TcpListener::bind(&sock_addr)
        .await
        .expect("Unable to bind to address!");

    axum::serve(listener, app)
        .await
        .expect("Unable to launch server!")
}

async fn hello() -> &'static str {
    "Hello from the server!"
}
