# Nix Templates

A collection of templates for Nix flakes.

## Usage

To populate the working directory with a template, run
```nix
nix flake init -t gitlab:kiako/nix-templates#<TEMPLATE>
```

Then, run the following command to enter the development environment:
```nix
nix develop
```

## Templates

The following templates are provided:

| Template | Description | Provides |
| -------- | ----------- | -------- |
| `bare` | Barebones development environment. | `just` |
| `bevy` | Latest stable Rust toolchain, with Bevy dependencies. | Rust toolchain. |
| `lua` | Lua 5.4. | `lua`, `luac` |
| `rust` | Latest stable Rust toolchain. | Rust toolchain. |
| `rust-fullstack` | Latest stable Rust toolchain for fullstack applications with Axum and Yew | Rust toolchain, `just`, `trunk`. |
| `typescript` | Typescript toolchain for targetting the browser. | `typescript`, `tsc`, `just` |
| `typst` | Typst, a PDF formatting languarge. | `typst` |
